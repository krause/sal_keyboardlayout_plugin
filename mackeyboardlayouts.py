from django.db.models import Count, F, Q

import sal.plugin

LAYOUT_Q = Q(facts__management_source__name='Puppet', facts__fact_name='mac_macbook_layout')

class MacKeyboardLayouts(sal.plugin.Widget):
    description = 'Chart of macbook keyboard layouts'
    supported_os_families = [sal.plugin.OSFamilies.darwin]

    def get_context(self, machines, **kwargs):
        context = self.super_get_context(machines, **kwargs)
        layouts = (machines.filter(LAYOUT_Q)
            .values(layout=F('facts__fact_data'))
            .annotate(count=Count('layout'))
            .order_by('count'))


        context['data'] = layouts
        return context

    def filter(self, machines, data):

        machines = machines.filter(LAYOUT_Q, facts__fact_data=data)
        return machines, data
